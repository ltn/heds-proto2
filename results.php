<?php

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use \PhpOffice\PhpSpreadsheet\Cell\Coordinate;

$lastVersion = filemtime("sheet.xlsx");

$force = isset($_GET['force']) ? $_GET['force'] : false;

if ($force || (filemtime('data.txt') < $lastVersion)) {
  $reader = new Xlsx($spreadsheet);
  $spreadsheet = $reader->load("sheet.xlsx");
  $worksheet = $spreadsheet->getActiveSheet();
  $array = $worksheet->toArray();
  $array = serialize($array);

  file_put_contents('array.txt', $array);
}

$array = file_get_contents('array.txt');
$array = unserialize($array);

$theme        = isset($_GET['theme']) ? trim($_GET['theme']) : false;
$pathology    = isset($_GET['pathology']) ? trim($_GET['pathology']) : false;
$organisation = isset($_GET['organisation']) ? trim($_GET['organisation']) : false;

$firstRow = 1;
$pathologyIndex = 0;
$themeIndex = 1;
$organisationIndex = 2;


for ($i = sizeof($array) - 1; $i >= $firstRow; $i--) {
  $unset = false;
      if ($theme) if (stripos($array[$i][$themeIndex], $theme) === FALSE) $unset = true;
      if ($pathology) if (stripos($array[$i][$pathologyIndex], $pathology) === FALSE) $unset = true;
      if ($organisation) if (stripos($array[$i][$organisationIndex], $organisation) === FALSE) $unset = true;
  if ($unset) {
    unset($array[$i]);
  }
}

$headers = array_shift($array);
$headerIndexes = array();
foreach ($headers as $headerIndex => $header) {
  $unset = true;
  foreach($array as $row) {
    if ($row[$headerIndex]) {
      $unset = false;
      break;
    }
  }
  if (!$unset) {
    $headerIndexes[] = $headerIndex;
  }
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
  <title>Moteur de recherche HEDS</title>
  <meta name="description" content="Prototype de moteur de recherche pour HEDS">
  <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
  <style type="text/css" media="screen"></style>
</head>
<body class="w-screen  text-xs m-auto">
  <div id="wrapper" class="h-screen">
    <div class="overflow-auto">
      <table class="table-auto">
        <thead>
          <tr>
            <?php foreach ($headerIndexes as $headerIndex): ?>
              <th class="px-4 py-2"><?php echo $headers[$headerIndex]; ?></th>
            <?php endforeach; ?>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($array as $row): ?>
          	<?php
				$test = trim(implode('', $row));
				if (!$test) {
					continue;
				}
          	?>
            <tr>
              <?php foreach ($headerIndexes as $headerIndex): ?>
                <?php $value = $row[$headerIndex]; ?>
                  <td class="border px-4 py-2 max-w-xs"><?php
                  if (stripos($value, 'http') === 0) {
                    echo '<a target="_blank" class="underline" href="'.$value.'">'.$value.'</a>';
                  } else {
                    echo $value;
                  }
                  ?></td>
              <?php endforeach; ?>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</body>
</html>
